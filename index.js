function playRound(playerChoice, computerChoice) {
  playerChoice = playerChoice.toLowerCase();
  computerChoice = computerChoice.toLowerCase();

  if (playerChoice === computerChoice) {
    return "It's a tie!";
  } else if (
    (playerChoice === "rock" && computerChoice === "scissors") ||
    (playerChoice === "paper" && computerChoice === "rock") ||
    (playerChoice === "scissors" && computerChoice === "paper")
  ) {
    return "You win!";
  } else {
    return "Computer wins!";
  }
}

function game() {
  const playerChoice = "ScIssor";
  let computerChoice;

  const predictableChoices = ["rock", "paper", "scissors"];

  let currentChoiceIndex = 0;

  computerChoice = predictableChoices[currentChoiceIndex];

  currentChoiceIndex = (currentChoiceIndex + 1) % predictableChoices.length;

  const roundResult = playRound(playerChoice, computerChoice);

  console.log(
    `You chose ${playerChoice}. Computer chose ${computerChoice}. ${roundResult}`
  );
}

game();
